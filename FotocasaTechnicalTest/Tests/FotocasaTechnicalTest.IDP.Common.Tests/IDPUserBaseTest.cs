﻿using FotocasaTechnicalTest.IDP.Common.Models;

namespace FotocasaTechnicalTest.IDP.Common.Tests
{
    public abstract class IDPUserBaseTest
    {

        protected static ApplicationUser GetDefaultUser()
        {
            return new ApplicationUser
            {
                NormalizedUserName = nameof(ApplicationUser.NormalizedUserName),
                UserName = nameof(ApplicationUser.UserName),
                PasswordHash = nameof(ApplicationUser.PasswordHash)
            };
        }
    }
}
