﻿using FotocasaTechnicalTest.IDP.Common.Models;

namespace FotocasaTechnicalTest.IDP.Common.Tests
{
    public abstract class IDPRoleBaseTest
    {

        protected static ApplicationRole GetDefaultRole()
        {
            return new ApplicationRole
            {
                Name = nameof(ApplicationRole.Name),
                NormalizedName = nameof(ApplicationRole.NormalizedName)
            };
        }
    }
}
