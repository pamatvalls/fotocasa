using FotocasaTechnicalTest.IDP.Common.Models;
using FotocasaTechnicalTest.IDP.Common.Tests;
using FotocasaTechnicalTest.IDP.InMemory.Stores;
using System;
using System.Linq;
using System.Threading;
using Xunit;

namespace FotocasaTechnicalTest.IDP.InMemory.Tests
{
    public class InMemoryRoleStoreTest : IDPRoleBaseTest, IDisposable
    {

        [Fact]
        public async void RoleStore_CreateAsync_AddsRoleToStore()
        {
            var role = GetDefaultRole();
            var roleStore = new InMemoryRoleStore();
            var cancellationToken = new CancellationToken();

            var result = await roleStore.CreateAsync(role, cancellationToken);

            Assert.True(result.Succeeded);
            Assert.Same(role, Context.InMemoryContext.Roles.First());
        }

        [Fact]
        public async void RoleStore_CreateAsyncWhenRoleExists_Fails()
        {
            var role = GetDefaultRole();
            var roleStore = new InMemoryRoleStore();
            var cancellationToken = new CancellationToken();
            Context.InMemoryContext.Roles.Add(role);

            var result = await roleStore.CreateAsync(role, cancellationToken);

            Assert.False(result.Succeeded);
            Assert.Same(role, Context.InMemoryContext.Roles.First());
        }

        [Fact]
        public async void RoleStore_DeleteAsync_DeletesRoleFromStore()
        {
            var role = GetDefaultRole();
            var roleStore = new InMemoryRoleStore();
            var cancellationToken = new CancellationToken();
            Context.InMemoryContext.Roles.Add(role);

            var result = await roleStore.DeleteAsync(role, cancellationToken);

            Assert.True(result.Succeeded);
        }

        [Fact]
        public async void RoleStore_DeleteAsyncWhenRoleNotExists_Fails()
        {
            var role = GetDefaultRole();
            var roleStore = new InMemoryRoleStore();
            var cancellationToken = new CancellationToken();

            var result = await roleStore.DeleteAsync(role, cancellationToken);

            Assert.False(result.Succeeded);
        }

        [Fact]
        public async void RoleStore_FindByIdAsync_ReturnsSameRole()
        {
            var role = GetDefaultRole();
            var roleStore = new InMemoryRoleStore();
            var cancellationToken = new CancellationToken();
            Context.InMemoryContext.Roles.Add(role);

            var storedRole = await roleStore.FindByIdAsync(role.Id, cancellationToken);

            Assert.Same(role, storedRole);
        }

        [Fact]
        public async void RoleStore_FindByNameAsync_ReturnsSameRole()
        {
            var role = GetDefaultRole();
            var roleStore = new InMemoryRoleStore();
            var cancellationToken = new CancellationToken();
            Context.InMemoryContext.Roles.Add(role);

            var storedRole = await roleStore.FindByNameAsync(role.NormalizedName, cancellationToken);

            Assert.Same(role, storedRole);
        }

        [Fact]
        public async void RoleStore_GetNormalizedRoleNameAsync_ReturnsNormalizedRoleName()
        {
            var role = GetDefaultRole();
            var roleStore = new InMemoryRoleStore();
            var cancellationToken = new CancellationToken();

            var normalizedRoleName = await roleStore.GetNormalizedRoleNameAsync(role, cancellationToken);

            Assert.Equal(nameof(ApplicationRole.NormalizedName), normalizedRoleName);
        }

        [Fact]
        public async void RoleStore_GetRoleIdAsync_ReturnsRoleName()
        {
            var role = GetDefaultRole();
            var roleStore = new InMemoryRoleStore();
            var cancellationToken = new CancellationToken();

            var roleId = await roleStore.GetRoleIdAsync(role, cancellationToken);

            Assert.Equal(role.Id, roleId);
        }

        [Fact]
        public async void RoleStore_GetRoleNameAsync_ReturnsRoleName()
        {
            var role = GetDefaultRole();
            var roleStore = new InMemoryRoleStore();
            var cancellationToken = new CancellationToken();

            var roleName = await roleStore.GetRoleNameAsync(role, cancellationToken);

            Assert.Equal(nameof(ApplicationRole.Name), roleName);
        }

        [Fact]
        public async void RoleStore_SetNormalizedRoleNameAsync_SetsNormalizedRoleName()
        {
            var role = GetDefaultRole();
            var roleStore = new InMemoryRoleStore();
            var cancellationToken = new CancellationToken();

            await roleStore.SetNormalizedRoleNameAsync(role, $"{nameof(ApplicationRole.NormalizedName)}_new", cancellationToken);

            Assert.Equal($"{nameof(ApplicationRole.NormalizedName)}_new", role.NormalizedName);
        }

        [Fact]
        public async void RoleStore_SetRoleNameAsync_SetsRoleName()
        {
            var role = GetDefaultRole();
            var roleStore = new InMemoryRoleStore();
            var cancellationToken = new CancellationToken();

            await roleStore.SetRoleNameAsync(role, $"{nameof(ApplicationRole.Name)}_new", cancellationToken);

            Assert.Equal($"{nameof(ApplicationRole.Name)}_new", role.Name);
        }

        [Fact]
        public async void RoleStore_UpdateAsync_UpdatesRoleInstance()
        {
            var role = GetDefaultRole();
            Context.InMemoryContext.Roles.Add(role);
            var newRole = GetDefaultRole();
            newRole.Id = role.Id;
            newRole.NormalizedName = $"{nameof(ApplicationRole.NormalizedName)}_new";
            newRole.Name = $"{nameof(ApplicationRole.Name)}_new";

            var roleStore = new InMemoryRoleStore();
            var cancellationToken = new CancellationToken();

            await roleStore.UpdateAsync(newRole, cancellationToken);

            Assert.Same(newRole, Context.InMemoryContext.Roles.First());
        }

        [Fact]
        public async void RoleStore_UpdateAsync_Fails()
        {
            var role = GetDefaultRole();
            Context.InMemoryContext.Roles.Add(role);
            var newRole = GetDefaultRole();

            var roleStore = new InMemoryRoleStore();
            var cancellationToken = new CancellationToken();

            var result = await roleStore.UpdateAsync(newRole, cancellationToken);

            Assert.False(result.Succeeded);
        }

        public void Dispose()
        {
            Context.InMemoryContext.Roles.Clear();
        }
    }
}
