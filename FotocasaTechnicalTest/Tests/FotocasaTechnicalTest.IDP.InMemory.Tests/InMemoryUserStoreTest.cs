using FotocasaTechnicalTest.IDP.Common.Models;
using FotocasaTechnicalTest.IDP.Common.Tests;
using FotocasaTechnicalTest.IDP.InMemory.Stores;
using System;
using System.Linq;
using System.Threading;
using Xunit;

namespace FotocasaTechnicalTest.IDP.InMemory.Tests
{
    public class InMemoryUserStoreTest : IDPUserBaseTest, IDisposable
    {
        [Fact]
        public async void UserStore_AddToRoleAsync_AddsRoleToUser()
        {
            var user = GetDefaultUser();
            var userStore = new InMemoryUserStore();
            var cancellationToken = new CancellationToken();
            Context.InMemoryContext.Users.Add(user);

            await userStore.AddToRoleAsync(user, nameof(ApplicationRole), cancellationToken);

            Assert.Equal(1, user.Roles.Count);
            Assert.Equal(nameof(ApplicationRole), user.Roles.First());
        }

        [Fact]
        public async void UserStore_CreateAsync_AddsUserToStore()
        {
            var user = GetDefaultUser();
            var userStore = new InMemoryUserStore();
            var cancellationToken = new CancellationToken();

            var result = await userStore.CreateAsync(user, cancellationToken);

            Assert.True(result.Succeeded);
            Assert.Same(user, Context.InMemoryContext.Users.First());
        }

        [Fact]
        public async void UserStore_CreateAsyncWhenUserExists_Fails()
        {
            var user = GetDefaultUser();
            var userStore = new InMemoryUserStore();
            var cancellationToken = new CancellationToken();
            Context.InMemoryContext.Users.Add(user);

            var result = await userStore.CreateAsync(user, cancellationToken);

            Assert.False(result.Succeeded);
            Assert.Same(user, Context.InMemoryContext.Users.First());
        }

        [Fact]
        public async void UserStore_DeleteAsync_DeletesUserFromStore()
        {
            var user = GetDefaultUser();
            var userStore = new InMemoryUserStore();
            var cancellationToken = new CancellationToken();
            Context.InMemoryContext.Users.Add(user);

            var result = await userStore.DeleteAsync(user, cancellationToken);

            Assert.True(result.Succeeded);
        }

        [Fact]
        public async void UserStore_DeleteAsyncWhenUserNotExists_Fails()
        {
            var user = GetDefaultUser();
            var userStore = new InMemoryUserStore();
            var cancellationToken = new CancellationToken();

            var result = await userStore.DeleteAsync(user, cancellationToken);

            Assert.False(result.Succeeded);
        }

        [Fact]
        public async void UserStore_FindByIdAsync_ReturnsSameUser()
        {
            var user = GetDefaultUser();
            var userStore = new InMemoryUserStore();
            var cancellationToken = new CancellationToken();
            Context.InMemoryContext.Users.Add(user);

            var storedUser = await userStore.FindByIdAsync(user.Id, cancellationToken);

            Assert.Same(user, storedUser);
        }

        [Fact]
        public async void UserStore_FindByNameAsync_ReturnsSameUser()
        {
            var user = GetDefaultUser();
            var userStore = new InMemoryUserStore();
            var cancellationToken = new CancellationToken();
            Context.InMemoryContext.Users.Add(user);

            var storedUser = await userStore.FindByNameAsync(user.NormalizedUserName, cancellationToken);

            Assert.Same(user, storedUser);
        }

        [Fact]
        public async void UserStore_GetNormalizedUserNameAsync_ReturnsNormalizedUserName()
        {
            var user = GetDefaultUser();
            var userStore = new InMemoryUserStore();
            var cancellationToken = new CancellationToken();

            var normalizedUserName = await userStore.GetNormalizedUserNameAsync(user, cancellationToken);

            Assert.Equal(nameof(ApplicationUser.NormalizedUserName), normalizedUserName);
        }

        [Fact]
        public async void UserStore_GetPasswordHashAsync_ReturnsPasswordHash()
        {
            var user = GetDefaultUser();
            var userStore = new InMemoryUserStore();
            var cancellationToken = new CancellationToken();

            var passwordHash = await userStore.GetPasswordHashAsync(user, cancellationToken);

            Assert.Equal(nameof(ApplicationUser.PasswordHash), passwordHash);
        }

        [Fact]
        public async void UserStore_GetRolesAsync_ReturnsUserRoles()
        {
            var user = GetDefaultUser();
            var userStore = new InMemoryUserStore();
            var cancellationToken = new CancellationToken();
            user.Roles.Add(nameof(ApplicationRole));

            var roles = await userStore.GetRolesAsync(user, cancellationToken);

            Assert.Equal(1, roles.Count);
            Assert.Equal(nameof(ApplicationRole), roles.First());
        }

        [Fact]
        public async void UserStore_GetUserIdAsync_ReturnsUserName()
        {
            var user = GetDefaultUser();
            var userStore = new InMemoryUserStore();
            var cancellationToken = new CancellationToken();

            var userId = await userStore.GetUserIdAsync(user, cancellationToken);

            Assert.Equal(user.Id, userId);
        }

        [Fact]
        public async void UserStore_GetUserNameAsync_ReturnsUserName()
        {
            var user = GetDefaultUser();
            var userStore = new InMemoryUserStore();
            var cancellationToken = new CancellationToken();

            var userName = await userStore.GetUserNameAsync(user, cancellationToken);

            Assert.Equal(nameof(ApplicationUser.UserName), userName);
        }

        [Fact]
        public async void UserStore_GetUsersInRoleAsync_ReturnsUser()
        {
            var user = GetDefaultUser();
            var userWithOtherRole = GetDefaultUser();
            var userStore = new InMemoryUserStore();
            var cancellationToken = new CancellationToken();
            user.Roles.Add(nameof(ApplicationRole));
            userWithOtherRole.Roles.Add($"{ nameof(ApplicationRole)}_2");
            Context.InMemoryContext.Users.Add(user);
            Context.InMemoryContext.Users.Add(userWithOtherRole);

            var users = await userStore.GetUsersInRoleAsync(nameof(ApplicationRole), cancellationToken);

            Assert.Equal(1, users.Count);
            Assert.Equal(user, users.First());
        }

        [Fact]
        public async void UserStore_HasPasswordAsync_ReturnsTrueWhenHasPassword()
        {
            var user = GetDefaultUser();
            var userStore = new InMemoryUserStore();
            var cancellationToken = new CancellationToken();

            var hasPassword = await userStore.HasPasswordAsync(user, cancellationToken);

            Assert.True(hasPassword);
        }

        [Fact]
        public async void UserStore_HasPasswordAsync_ReturnsFalsWhenEmptyPassword()
        {
            var user = GetDefaultUser();
            user.PasswordHash = string.Empty;
            var userStore = new InMemoryUserStore();
            var cancellationToken = new CancellationToken();

            var hasPassword = await userStore.HasPasswordAsync(user, cancellationToken);

            Assert.False(hasPassword);
        }

        [Fact]
        public async void UserStore_IsInRoleAsync_ReturnsTrueWhenUserHasRole()
        {
            var user = GetDefaultUser();
            var userStore = new InMemoryUserStore();
            var cancellationToken = new CancellationToken();
            user.Roles.Add(nameof(ApplicationRole));

            var isInRole = await userStore.IsInRoleAsync(user, nameof(ApplicationRole), cancellationToken);

            Assert.True(isInRole);
        }

        [Fact]
        public async void UserStore_IsInRoleAsync_ReturnsTrueWhenUserHasNotRole()
        {
            var user = GetDefaultUser();
            var userStore = new InMemoryUserStore();
            var cancellationToken = new CancellationToken();

            var isInRole = await userStore.IsInRoleAsync(user, nameof(ApplicationRole), cancellationToken);

            Assert.False(isInRole);
        }

        [Fact]
        public async void UserStore_RemoveFromRoleAsync_RemovesRoleFromUser()
        {
            var user = GetDefaultUser();
            var userStore = new InMemoryUserStore();
            var cancellationToken = new CancellationToken();
            user.Roles.Add(nameof(ApplicationRole));

            await userStore.RemoveFromRoleAsync(user, nameof(ApplicationRole), cancellationToken);

            Assert.Equal(0, user.Roles.Count);
        }

        [Fact]
        public async void UserStore_SetNormalizedUserNameAsync_SetsNormalizedUserName()
        {
            var user = GetDefaultUser();
            var userStore = new InMemoryUserStore();
            var cancellationToken = new CancellationToken();

            await userStore.SetNormalizedUserNameAsync(user, $"{nameof(ApplicationUser.NormalizedUserName)}_new", cancellationToken);

            Assert.Equal($"{nameof(ApplicationUser.NormalizedUserName)}_new", user.NormalizedUserName);
        }

        [Fact]
        public async void UserStore_SetPasswordHashAsync_SetsPasswordHash()
        {
            var user = GetDefaultUser();
            var userStore = new InMemoryUserStore();
            var cancellationToken = new CancellationToken();

            await userStore.SetPasswordHashAsync(user, $"{nameof(ApplicationUser.PasswordHash)}_new", cancellationToken);

            Assert.Equal($"{nameof(ApplicationUser.PasswordHash)}_new", user.PasswordHash);
        }

        [Fact]
        public async void UserStore_SetUserNameAsync_SetsUserName()
        {
            var user = GetDefaultUser();
            var userStore = new InMemoryUserStore();
            var cancellationToken = new CancellationToken();

            await userStore.SetUserNameAsync(user, $"{nameof(ApplicationUser.UserName)}_new", cancellationToken);

            Assert.Equal($"{nameof(ApplicationUser.UserName)}_new", user.UserName);
        }

        [Fact]
        public async void UserStore_UpdateAsync_UpdatesUserInstance()
        {
            var user = GetDefaultUser();
            Context.InMemoryContext.Users.Add(user);
            var newUser = GetDefaultUser();
            newUser.Id = user.Id;
            newUser.NormalizedUserName = $"{nameof(ApplicationUser.NormalizedUserName)}_new";
            newUser.PasswordHash = $"{nameof(ApplicationUser.PasswordHash)}_new";
            newUser.UserName = $"{nameof(ApplicationUser.UserName)}_new";

            var userStore = new InMemoryUserStore();
            var cancellationToken = new CancellationToken();

            var result = await userStore.UpdateAsync(newUser, cancellationToken);

            Assert.Same(newUser, Context.InMemoryContext.Users.First());
            Assert.True(result.Succeeded);
        }

        [Fact]
        public async void UserStore_UpdateAsync_Fails()
        {
            var user = GetDefaultUser();
            Context.InMemoryContext.Users.Add(user);
            var newUser = GetDefaultUser();

            var userStore = new InMemoryUserStore();
            var cancellationToken = new CancellationToken();

            var result = await userStore.UpdateAsync(newUser, cancellationToken);

            Assert.False(result.Succeeded);
        }

        public void Dispose()
        {
            Context.InMemoryContext.Users.Clear();
        }
    }
}
