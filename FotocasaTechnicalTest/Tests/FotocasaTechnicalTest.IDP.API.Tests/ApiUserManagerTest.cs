using FotocasaTechnicalTest.IDP.API.Managers;
using FotocasaTechnicalTest.IDP.API.Stores;
using FotocasaTechnicalTest.IDP.Common.Models;
using FotocasaTechnicalTest.IDP.Common.Tests;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace FotocasaTechnicalTest.IDP.API.Tests
{
    public class ApiUserManagerTest : IDPUserBaseTest, IDisposable
    {
        private readonly Mock<ILogger<ApiUserManager>> loggerMock;
        private readonly Mock<ICustomUserStore<ApplicationUser>> userStoreMock;
        private readonly Mock<IOptions<IdentityOptions>> identityOptionsMock;
        private readonly Mock<IPasswordHasher<ApplicationUser>> passwordHasherMock;
        private readonly Mock<IEnumerable<IUserValidator<ApplicationUser>>> userValidatorsMock;
        private readonly Mock<IEnumerable<IPasswordValidator<ApplicationUser>>> passwordValidatorsMock;
        private readonly Mock<ILookupNormalizer> keyNormalizerMock;
        private readonly Mock<IServiceProvider> servicesMock;

        private readonly ApiUserManager userManager;
        public ApiUserManagerTest()
        {
            userStoreMock = new Mock<ICustomUserStore<ApplicationUser>>(MockBehavior.Strict);
            loggerMock = new Mock<ILogger<ApiUserManager>>(MockBehavior.Loose);
            identityOptionsMock = new Mock<IOptions<IdentityOptions>>(MockBehavior.Loose);
            passwordHasherMock = new Mock<IPasswordHasher<ApplicationUser>>(MockBehavior.Loose);
            userValidatorsMock = new Mock<IEnumerable<IUserValidator<ApplicationUser>>>(MockBehavior.Loose) { DefaultValue = DefaultValue.Mock };
            passwordValidatorsMock = new Mock<IEnumerable<IPasswordValidator<ApplicationUser>>>(MockBehavior.Loose) { DefaultValue = DefaultValue.Mock };
            keyNormalizerMock = new Mock<ILookupNormalizer>(MockBehavior.Loose) { DefaultValue = DefaultValue.Mock };
            servicesMock = new Mock<IServiceProvider>(MockBehavior.Loose) { DefaultValue = DefaultValue.Mock };
            userManager = new ApiUserManager(userStoreMock.Object, identityOptionsMock.Object, passwordHasherMock.Object, userValidatorsMock.Object, passwordValidatorsMock.Object, keyNormalizerMock.Object, new IdentityErrorDescriber(), servicesMock.Object, loggerMock.Object);
        }

        [Fact]
        public async void ApiUserManager_FindByNameAndPasswordAsync_GetsUser()
        {
            var userName = "userName";
            var password = "password";
            var defaultUser = GetDefaultUser();

            userStoreMock.Setup(methods => methods.FindByNameAndPasswordAsync(userName, password, It.IsAny<CancellationToken>())).Returns(Task.FromResult(defaultUser));
            keyNormalizerMock.Setup(methods => methods.Normalize(userName)).Returns(userName);

            var user = await userManager.FindByNameAndPasswordAsync(userName, password);

            Assert.Same(defaultUser, user);
        }



        public void Dispose()
        {
        }
    }
}
