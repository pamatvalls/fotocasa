using FotocasaTechnicalTest.IDP.API.Context;
using FotocasaTechnicalTest.IDP.API.Stores;
using FotocasaTechnicalTest.IDP.Common.Tests;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace FotocasaTechnicalTest.IDP.API.Tests
{
    public class ApiUserStoreTest : IDPUserBaseTest, IDisposable
    {
        private readonly Mock<IContext> contextMock;
        private readonly Mock<ILogger<ApiUserStore>> loggerMock;
        private readonly ApiUserStore userStore;
        public ApiUserStoreTest()
        {
            contextMock = new Mock<IContext>(MockBehavior.Strict);
            loggerMock = new Mock<ILogger<ApiUserStore>>(MockBehavior.Loose);
            userStore = new ApiUserStore(contextMock.Object, loggerMock.Object);
        }

        [Fact]
        public async void UserStore_GetUserByNormalizedUserNameAndPassword_GetsUser()
        {
            var cancellationToken = new CancellationToken();
            var userName = "userName";
            var password = "password";
            var defaultUser = GetDefaultUser();

            contextMock.Setup(methods => methods.GetUserByNormalizedUserNameAndPassword(userName, password)).Returns(Task.FromResult(defaultUser));

            var user = await userStore.FindByNameAndPasswordAsync(userName, password, cancellationToken);

            Assert.Same(defaultUser, user);
        }

        public void Dispose()
        {
        }
    }
}
