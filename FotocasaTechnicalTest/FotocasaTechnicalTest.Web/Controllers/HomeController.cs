﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using FotocasaTechnicalTest.Web.Models;
using Microsoft.AspNetCore.Authorization;

namespace FotocasaTechnicalTest.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "PAGE_1,ADMIN")]
        public IActionResult Page1()
        {
            return View();
        }

        [Authorize(Roles = "PAGE_2,ADMIN")]
        public IActionResult Page2()
        {
            return View();
        }

        [Authorize(Roles = "PAGE_3,ADMIN")]
        public IActionResult Page3()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
