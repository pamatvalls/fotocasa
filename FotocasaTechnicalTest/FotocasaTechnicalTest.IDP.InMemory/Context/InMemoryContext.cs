﻿using FotocasaTechnicalTest.IDP.Common.Models;
using System.Collections.Generic;

namespace FotocasaTechnicalTest.IDP.InMemory.Context
{
    internal static class InMemoryContext
    {
        internal static readonly List<ApplicationUser> Users = new List<ApplicationUser>();

        internal static readonly List<ApplicationRole> Roles = new List<ApplicationRole>();
    }
}
