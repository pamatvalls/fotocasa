﻿using FotocasaTechnicalTest.IDP.Common.Models;
using FotocasaTechnicalTest.IDP.InMemory.Context;
using Microsoft.AspNetCore.Identity;
using FotocasaTechnicalTest.IDP.Common.Stores;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FotocasaTechnicalTest.IDP.InMemory.Stores
{
    internal class InMemoryRoleStore : BaseRoleStore, IRoleStore<ApplicationRole>
    {
        public override Task<IdentityResult> CreateAsync(ApplicationRole role, CancellationToken cancellationToken)
        {
            if (!InMemoryContext.Roles.Any(x => x.Id == role.Id))
            {
                InMemoryContext.Roles.Add(role);
            }
            else
            {
                return Task.FromResult(IdentityResult.Failed());
            }
            return Task.FromResult(IdentityResult.Success);
        }

        public override Task<IdentityResult> DeleteAsync(ApplicationRole role, CancellationToken cancellationToken)
        {
            var storedRole = InMemoryContext.Roles.FirstOrDefault(x => x.Id == role.Id);
            if (storedRole != null)
            {
                InMemoryContext.Roles.Remove(storedRole);
            }
            else
            {
                return Task.FromResult(IdentityResult.Failed());
            }
            return Task.FromResult(IdentityResult.Success);
        }

        public override void Dispose()
        {

        }

        public override Task<ApplicationRole> FindByIdAsync(string roleId, CancellationToken cancellationToken)
        {
            return Task.FromResult(InMemoryContext.Roles.FirstOrDefault(x => x.Id == roleId));
        }

        public override Task<ApplicationRole> FindByNameAsync(string normalizedRoleName, CancellationToken cancellationToken)
        {
            return Task.FromResult(InMemoryContext.Roles.FirstOrDefault(x => x.NormalizedName == normalizedRoleName));
        }

        public override Task<IdentityResult> UpdateAsync(ApplicationRole role, CancellationToken cancellationToken)
        {
            var storedRole = InMemoryContext.Roles.FirstOrDefault(x => x.Id == role.Id);
            if (storedRole != null)
            {
                InMemoryContext.Roles.Remove(storedRole);
                InMemoryContext.Roles.Add(role);
                return Task.FromResult(IdentityResult.Success);
            }
            return Task.FromResult(IdentityResult.Failed());
        }
    }
}
