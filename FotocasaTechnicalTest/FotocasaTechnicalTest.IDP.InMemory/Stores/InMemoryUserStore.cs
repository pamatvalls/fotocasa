﻿using FotocasaTechnicalTest.IDP.Common.Models;
using FotocasaTechnicalTest.IDP.Common.Stores;
using FotocasaTechnicalTest.IDP.InMemory.Context;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FotocasaTechnicalTest.IDP.InMemory.Stores
{
    internal class InMemoryUserStore : BaseUserStore,
                        IQueryableUserStore<ApplicationUser>,
                        IUserRoleStore<ApplicationUser>,
                        IUserPasswordStore<ApplicationUser>
    {
        public IQueryable<ApplicationUser> Users => InMemoryContext.Users.AsQueryable();

        public override Task<IdentityResult> CreateAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            if (!InMemoryContext.Users.Any(x => x.Id == user.Id))
            {
                InMemoryContext.Users.Add(user);
            }
            else
            {
                return Task.FromResult(IdentityResult.Failed());
            }
            return Task.FromResult(IdentityResult.Success);
        }

        public override Task<IdentityResult> DeleteAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            var storedUser = InMemoryContext.Users.FirstOrDefault(x => x.Id == user.Id);
            if (storedUser != null)
            {
                InMemoryContext.Users.Remove(storedUser);
            }
            else
            {
                return Task.FromResult(IdentityResult.Failed());
            }
            return Task.FromResult(IdentityResult.Success);
        }

        public override void Dispose()
        {
        }

        public override Task<ApplicationUser> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            return Task.FromResult(InMemoryContext.Users.FirstOrDefault(x => x.Id == userId));
        }

        public override Task<ApplicationUser> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            return Task.FromResult(InMemoryContext.Users.FirstOrDefault(x => x.NormalizedUserName == normalizedUserName));
        }

        public override Task<IList<ApplicationUser>> GetUsersInRoleAsync(string roleName, CancellationToken cancellationToken)
        {
            return Task.FromResult((IList<ApplicationUser>)InMemoryContext.Users.Where(x => x.Roles.Contains(roleName)).ToList());
        }

        public override Task<IdentityResult> UpdateAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            var storedUser = InMemoryContext.Users.FirstOrDefault(x => x.Id == user.Id);
            if (storedUser != null)
            {
                InMemoryContext.Users.Remove(storedUser);
                InMemoryContext.Users.Add(user);
                return Task.FromResult(IdentityResult.Success);
            }
            return Task.FromResult(IdentityResult.Failed());
        }
    }
}
