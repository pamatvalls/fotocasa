﻿using FotocasaTechnicalTest.IDP.Common.Models;
using FotocasaTechnicalTest.IDP.InMemory.Stores;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("FotocasaTechnicalTest.IDP.InMemory.Tests")]
namespace FotocasaTechnicalTest.IDP.InMemory
{
    public static class ServicesExtensions
    {
        public static IServiceCollection AddInMemoryIdentity(this IServiceCollection services)
        {
            services.AddIdentityCore<ApplicationUser>(config =>
            {
                config.Password.RequireLowercase = false;
                config.Password.RequiredLength = 5;
                config.Password.RequireNonAlphanumeric = false;
                config.Password.RequireDigit = false;
            })
                .AddRoles<ApplicationRole>()
                .AddUserStore<InMemoryUserStore>()
                .AddRoleStore<InMemoryRoleStore>();
            services.AddHttpContextAccessor();
            services.TryAddScoped<SignInManager<ApplicationUser>>();

            return services;
        }
    }
}
