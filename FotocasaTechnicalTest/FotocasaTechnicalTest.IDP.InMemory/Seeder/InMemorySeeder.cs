﻿using FotocasaTechnicalTest.IDP.Common.Models;
using Microsoft.AspNetCore.Identity;

namespace FotocasaTechnicalTest.IDP.InMemory.Seeder
{
    public static class InMemorySeeder
    {
        public static void SeedUsersAndRoles(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            SeedRoles(roleManager);
            SeedUsers(userManager);
        }

        private static void SeedRoles(RoleManager<ApplicationRole> roleManager)
        {
            CreateRoleIfNotExists(roleManager, "PAGE_1");
            CreateRoleIfNotExists(roleManager, "PAGE_2");
            CreateRoleIfNotExists(roleManager, "PAGE_3");
            CreateRoleIfNotExists(roleManager, "ADMIN");
        }

        private static void CreateRoleIfNotExists(RoleManager<ApplicationRole> roleManager, string roleName)
        {
            if (!roleManager.RoleExistsAsync(roleName).Result)
            {
                var role = new ApplicationRole
                {
                    Name = roleName
                };
                var result = roleManager.CreateAsync(role).Result;
            }
        }

        private static void SeedUsers(UserManager<ApplicationUser> userManager)
        {
            CreateUserIfNotExists(userManager, "PAGE_1");
            CreateUserIfNotExists(userManager, "PAGE_2");
            CreateUserIfNotExists(userManager, "PAGE_3");
            CreateUserIfNotExists(userManager, "ADMIN");
        }

        private static void CreateUserIfNotExists(UserManager<ApplicationUser> userManager, string userName)
        {
            var user = new ApplicationUser
            {
                UserName = userName
            };

            var result = userManager.CreateAsync(user, userName).Result;

            if (result.Succeeded)
            {
                userManager.AddToRoleAsync(user, userName).Wait();
            }
        }
    }
}
