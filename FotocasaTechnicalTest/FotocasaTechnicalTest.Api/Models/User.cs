﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FotocasaTechnicalTest.Api.Models
{
    public class User
    {
        public string Id { get; set; }

        public string UserName { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        public string Password { get; set; }

        [JsonProperty("password")]
        private string PasswordAlternateSetter
        {
            set { Password = value; }
        }

        public IList<string> Roles { get; set; }
    }
}
