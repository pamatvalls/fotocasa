﻿using AutoMapper;
using FotocasaTechnicalTest.Api.Models;
using FotocasaTechnicalTest.IDP.Common.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FotocasaTechnicalTest.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    [Produces("application/json", "application/xml")]
    public class UsersController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IMapper mapper;
        private readonly ILogger<UsersController> logger;

        public UsersController(UserManager<ApplicationUser> userManager, IMapper mapper, ILogger<UsersController> logger)
        {
            this.userManager = userManager;
            this.mapper = mapper;
            this.logger = logger;
        }

        // GET: api/Users
        [HttpGet]
        public ActionResult<IEnumerable<User>> Get()
        {
            return Ok(userManager.Users.Select(mapper.Map<User>));
        }

        // GET: api/Users/5
        [HttpGet("{id}", Name = "Get")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<User>> Get(string id)
        {
            var user = await userManager.FindByIdAsync(id);
            if (user == null)
            {
                logger.LogError("User {userId} not found", id);
                return NotFound();
            }
            return Ok(mapper.Map<User>(user));
        }

        // GET: api/Users?userName={userName}
        [HttpGet("withname/{userName}", Name = "GetByName")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<User>> GetByName(string userName)
        {
            var user = await userManager.FindByNameAsync(userName);
            if (user == null)
            {
                logger.LogError("User {userName} not found", userName);
                return NotFound();
            }
            return Ok(mapper.Map<User>(user));
        }

        // POST: api/Users
        [HttpPost]
        [Authorize(Roles = "ADMIN")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult> Post([FromBody] User value)
        {
            var result = await userManager.CreateAsync(mapper.Map<ApplicationUser>(value), value.Password);

            if (!result.Succeeded)
            {
                logger.LogError("Error Creating user {user}", value.UserName);
                return BadRequest();
            }
            return Ok();
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        [Authorize(Roles = "ADMIN")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<ActionResult> Put(string id, [FromBody] User value)
        {
            var user = await userManager.FindByIdAsync(id);
            if (user == null)
            {
                logger.LogError("User {userId} not found", id);
                return NotFound();
            }
            mapper.Map(value, user);

            if (!string.IsNullOrEmpty(value.Password))
            {
                user.PasswordHash = userManager.PasswordHasher.HashPassword(user, value.Password);
            }

            var result = await userManager.UpdateAsync(user);

            if (!result.Succeeded)
            {
                logger.LogError("Error Updating user {user}", user.UserName);
                return BadRequest();
            }
            return Ok();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "ADMIN")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<ActionResult> Delete(string id)
        {
            var user = await userManager.FindByIdAsync(id);
            if (user == null)
            {
                logger.LogError("User {userId} not found", id);
                return NotFound();
            }

            var result = await userManager.DeleteAsync(user);

            if (!result.Succeeded)
            {
                logger.LogError("Error Deleting user {user}", user.UserName);
                return BadRequest();
            }
            return Ok();
        }
    }
}
