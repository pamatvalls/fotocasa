﻿using FotocasaTechnicalTest.IDP.Common.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace FotocasaTechnicalTest.Api
{
    public class BasicAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly UserManager<ApplicationUser> userManager;

        public BasicAuthenticationHandler(
            IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            SignInManager<ApplicationUser> signInManager,
            UserManager<ApplicationUser> userManager)
            : base(options, logger, encoder, clock)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.ContainsKey("Authorization"))
                return AuthenticateResult.Fail("Missing Authorization Header");

            try
            {
                var authHeader = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
                var credentialBytes = Convert.FromBase64String(authHeader.Parameter);
                var credentials = Encoding.UTF8.GetString(credentialBytes).Split(':');
                var username = credentials[0];
                var password = credentials[1];
                var identityUser = await userManager.FindByNameAsync(username);

                if (identityUser != null)
                {
                    var signInResult = await userManager.CheckPasswordAsync(identityUser, password);
                    if (signInResult)
                    {
                        var ticket = new AuthenticationTicket(await signInManager.ClaimsFactory.CreateAsync(identityUser), Scheme.Name);
                        return AuthenticateResult.Success(ticket);
                    }

                }
                return AuthenticateResult.Fail("Invalid User or Password");
            }
            catch
            {
                return AuthenticateResult.Fail("Invalid Authorization Header");
            }



        }
    }
}