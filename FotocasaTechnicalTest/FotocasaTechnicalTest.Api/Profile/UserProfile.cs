﻿using FotocasaTechnicalTest.IDP.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FotocasaTechnicalTest.Api.Profile
{
    public class UserProfile : AutoMapper.Profile
    {

        public UserProfile()
        {
            CreateMap<ApplicationUser, Models.User>();
            CreateMap<Models.User, ApplicationUser>()                
                .ForMember(x => x.Id, options => options.UseDestinationValue());
        }
    }
}
