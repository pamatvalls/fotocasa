﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace FotocasaTechnicalTest.IDP.Common.Models
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser() : base()
        {
            Roles = new List<string>();
        }
        public IList<string> Roles { get; private set; }
    }
}