﻿using Microsoft.AspNetCore.Identity;

namespace FotocasaTechnicalTest.IDP.Common.Models
{
    public class ApplicationRole : IdentityRole
    {
    }
}
