﻿using FotocasaTechnicalTest.IDP.Common.Models;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FotocasaTechnicalTest.IDP.Common.Stores
{
    public abstract class BaseUserStore : IUserStore<ApplicationUser>,
                         IUserRoleStore<ApplicationUser>,
                         IUserPasswordStore<ApplicationUser>
    {

        public Task AddToRoleAsync(ApplicationUser user, string roleName, CancellationToken cancellationToken)
        {
            user.Roles.Add(roleName);

            return Task.CompletedTask;
        }

        public abstract Task<IdentityResult> CreateAsync(ApplicationUser user, CancellationToken cancellationToken);


        public abstract Task<IdentityResult> DeleteAsync(ApplicationUser user, CancellationToken cancellationToken);

        public abstract void Dispose();

        public abstract Task<ApplicationUser> FindByIdAsync(string userId, CancellationToken cancellationToken);

        public abstract Task<ApplicationUser> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken);

        public Task<string> GetNormalizedUserNameAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.NormalizedUserName);
        }

        public Task<string> GetPasswordHashAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.PasswordHash);
        }

        public Task<IList<string>> GetRolesAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Roles);
        }

        public Task<string> GetUserIdAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Id);
        }

        public Task<string> GetUserNameAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.UserName);
        }

        public abstract Task<IList<ApplicationUser>> GetUsersInRoleAsync(string roleName, CancellationToken cancellationToken);

        public Task<bool> HasPasswordAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(!string.IsNullOrWhiteSpace(user.PasswordHash));
        }

        public Task<bool> IsInRoleAsync(ApplicationUser user, string roleName, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Roles.Any(x => x == roleName));
        }

        public Task RemoveFromRoleAsync(ApplicationUser user, string roleName, CancellationToken cancellationToken)
        {
            user.Roles.Remove(roleName);

            return Task.CompletedTask;
        }

        public Task SetNormalizedUserNameAsync(ApplicationUser user, string normalizedName, CancellationToken cancellationToken)
        {
            user.NormalizedUserName = normalizedName;

            return Task.CompletedTask;
        }

        public Task SetPasswordHashAsync(ApplicationUser user, string passwordHash, CancellationToken cancellationToken)
        {
            user.PasswordHash = passwordHash;

            return Task.CompletedTask;
        }

        public Task SetUserNameAsync(ApplicationUser user, string userName, CancellationToken cancellationToken)
        {
            user.UserName = userName;

            return Task.CompletedTask;
        }

        public abstract Task<IdentityResult> UpdateAsync(ApplicationUser user, CancellationToken cancellationToken);
    }
}
