﻿using FotocasaTechnicalTest.IDP.Common.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FotocasaTechnicalTest.IDP.Common.Stores
{
    public abstract class BaseRoleStore : IRoleStore<ApplicationRole>
    {
        public abstract Task<IdentityResult> CreateAsync(ApplicationRole role, CancellationToken cancellationToken);

        public abstract Task<IdentityResult> DeleteAsync(ApplicationRole role, CancellationToken cancellationToken);

        public abstract void Dispose();

        public abstract Task<ApplicationRole> FindByIdAsync(string roleId, CancellationToken cancellationToken);

        public abstract Task<ApplicationRole> FindByNameAsync(string normalizedRoleName, CancellationToken cancellationToken);

        public Task<string> GetNormalizedRoleNameAsync(ApplicationRole role, CancellationToken cancellationToken)
        {
            return Task.FromResult(role.NormalizedName);
        }

        public Task<string> GetRoleIdAsync(ApplicationRole role, CancellationToken cancellationToken)
        {
            return Task.FromResult(role.Id);
        }

        public Task<string> GetRoleNameAsync(ApplicationRole role, CancellationToken cancellationToken)
        {
            return Task.FromResult(role.Name);
        }

        public Task SetNormalizedRoleNameAsync(ApplicationRole role, string normalizedName, CancellationToken cancellationToken)
        {
            role.NormalizedName = normalizedName;
            return Task.CompletedTask;
        }

        public Task SetRoleNameAsync(ApplicationRole role, string roleName, CancellationToken cancellationToken)
        {
            role.Name = roleName;
            return Task.CompletedTask;
        }

        public abstract Task<IdentityResult> UpdateAsync(ApplicationRole role, CancellationToken cancellationToken);
    }
}
