﻿using FotocasaTechnicalTest.IDP.API.Context;
using FotocasaTechnicalTest.IDP.Common.Models;
using FotocasaTechnicalTest.IDP.Common.Stores;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace FotocasaTechnicalTest.IDP.API.Stores
{
    internal class ApiUserStore : BaseUserStore,
                        ICustomUserStore<ApplicationUser>,
                        IUserPasswordStore<ApplicationUser>
    {
        private readonly IContext context;
        private readonly ILogger<ApiUserStore> logger;
        public ApiUserStore(IContext context, ILogger<ApiUserStore> logger)
        {
            this.context = context;
            this.logger = logger;
        }

        public override Task<IdentityResult> CreateAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public override Task<IdentityResult> DeleteAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public override void Dispose()
        {
            context.Dispose();
        }

        public override Task<ApplicationUser> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public async Task<ApplicationUser> FindByNameAndPasswordAsync(string userName, string password, CancellationToken cancellationToken)
        {
            return await context.GetUserByNormalizedUserNameAndPassword(userName, password);
        }

        public override Task<ApplicationUser> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public override Task<IList<ApplicationUser>> GetUsersInRoleAsync(string roleName, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public override Task<IdentityResult> UpdateAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }
    }
}
