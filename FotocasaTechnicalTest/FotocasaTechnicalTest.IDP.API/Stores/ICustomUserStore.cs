﻿using System.Threading;
using System.Threading.Tasks;
using FotocasaTechnicalTest.IDP.Common.Models;
using Microsoft.AspNetCore.Identity;

namespace FotocasaTechnicalTest.IDP.API.Stores
{
    public interface ICustomUserStore<TUser> : IUserRoleStore<TUser> where TUser : class
    {
        Task<ApplicationUser> FindByNameAndPasswordAsync(string userName, string password, CancellationToken cancellationToken);
    }
}
