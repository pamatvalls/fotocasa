﻿using FotocasaTechnicalTest.IDP.Common.Models;
using Microsoft.AspNetCore.Identity;
using FotocasaTechnicalTest.IDP.Common.Stores;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using FotocasaTechnicalTest.IDP.API.Context;

namespace FotocasaTechnicalTest.IDP.API.Stores
{
    internal class ApiRoleStore : BaseRoleStore, IRoleStore<ApplicationRole>
    {

        private readonly IContext context;
        private readonly ILogger<ApiRoleStore> logger;
        public ApiRoleStore(IContext context, ILogger<ApiRoleStore> logger)
        {
            this.context = context;
            this.logger = logger;
        }

        public override Task<IdentityResult> CreateAsync(ApplicationRole role, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public override Task<IdentityResult> DeleteAsync(ApplicationRole role, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public override void Dispose()
        {
            context.Dispose();
        }

        public override Task<ApplicationRole> FindByIdAsync(string roleId, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public override Task<ApplicationRole> FindByNameAsync(string normalizedRoleName, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public override Task<IdentityResult> UpdateAsync(ApplicationRole role, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }
    }
}
