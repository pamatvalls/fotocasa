﻿using FotocasaTechnicalTest.IDP.Common.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using FotocasaTechnicalTest.IDP.API.Stores;

namespace FotocasaTechnicalTest.IDP.API.Managers
{
    public class ApiUserManager : UserManager<ApplicationUser>
    {
        private readonly ICustomUserStore<ApplicationUser> store;

        public ApiUserManager(ICustomUserStore<ApplicationUser> store, IOptions<IdentityOptions> optionsAccessor, IPasswordHasher<ApplicationUser> passwordHasher, IEnumerable<IUserValidator<ApplicationUser>> userValidators, IEnumerable<IPasswordValidator<ApplicationUser>> passwordValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, IServiceProvider services, ILogger<UserManager<ApplicationUser>> logger) : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
            this.store = store;
        }

        public async Task<ApplicationUser> FindByNameAndPasswordAsync(string userName, string password)
        {
            ThrowIfDisposed();
            if (userName == null)
            {
                throw new ArgumentNullException(nameof(userName));
            }
            userName = NormalizeKey(userName);
            return await store.FindByNameAndPasswordAsync(userName, password, CancellationToken);
        }

    }
}
