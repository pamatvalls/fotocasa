﻿using System.Threading.Tasks;
using FotocasaTechnicalTest.IDP.Common.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace FotocasaTechnicalTest.IDP.API.Managers
{
    public class ApiSignInManager : SignInManager<ApplicationUser>
    {
        private readonly ApiUserManager userManager;
        private readonly ILogger<SignInManager<ApplicationUser>> logger;

        public ApiSignInManager(ApiUserManager userManager, IHttpContextAccessor contextAccessor, IUserClaimsPrincipalFactory<ApplicationUser> claimsFactory, IOptions<IdentityOptions> optionsAccessor, ILogger<SignInManager<ApplicationUser>> logger, IAuthenticationSchemeProvider schemes) : base(userManager, contextAccessor, claimsFactory, optionsAccessor, logger, schemes)
        {
            this.userManager = userManager;
            this.logger = logger;
        }

        public async override Task<SignInResult> PasswordSignInAsync(string userName, string password, bool isPersistent, bool lockoutOnFailure)
        {
            var user = await userManager.FindByNameAndPasswordAsync(userName, password);
            if (user == null)
            {
                logger.LogError("Failed login for user {user}", userName);
                return SignInResult.Failed;
            }
            await SignInAsync(user, new AuthenticationProperties
            {
                IsPersistent = isPersistent
            }, null);

            return SignInResult.Success;
        }
    }
}
