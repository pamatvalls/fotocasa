﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FotocasaTechnicalTest.IDP.API.Infrastructure
{
    internal static class API
    {

        internal static class User
        {
            internal static string GetAllUsers(string baseUri) => $"{baseUri}/users";
            internal static string GetUser(string baseUri, string userId) => $"{baseUri}/users/{userId}";
            internal static string DeleteUser(string baseUri, string userId) => $"{baseUri}/users/{userId}";
            internal static string UpdateUser(string baseUri, string userId) => $"{baseUri}/users/{userId}";
            internal static string AddUser(string baseUri) => $"{baseUri}/users";
            internal static string GetUserByName(string baseUri, string userName) => $"{baseUri}/users/withname/{userName}";
            internal static string GetUserPassword(string baseUri, string userId) => $"{baseUri}/users/{userId}/password";
        }

        public static class Role
        {
        }
    }
}
