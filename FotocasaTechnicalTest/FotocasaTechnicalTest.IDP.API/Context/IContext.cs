﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FotocasaTechnicalTest.IDP.Common.Models;

namespace FotocasaTechnicalTest.IDP.API.Context
{
    public interface IContext : IDisposable
    {
        Task<ApplicationUser> GetUserByNormalizedUserNameAndPassword(string normalizedUserName, string password);
    }
}