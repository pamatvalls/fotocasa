﻿using FotocasaTechnicalTest.IDP.Common.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace FotocasaTechnicalTest.IDP.API.Context
{
    internal class ApiContext : IContext
    {
        private readonly HttpClient httpClient;
        private readonly ILogger<ApiContext> logger;

        private string BaseUri => "http://fotocasatechnicaltest.api/api";
        public ApiContext(HttpClient httpClient, ILogger<ApiContext> logger)
        {
            this.httpClient = httpClient;
            this.logger = logger;
        }

        public Task AddRole(ApplicationRole role)
        {
            throw new NotImplementedException();
        }

        public Task AddUser(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task DeleteRole(ApplicationRole role)
        {
            throw new NotImplementedException();
        }

        public Task DeleteUser(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task<ApplicationRole> GetRoleById(string roleId)
        {
            throw new NotImplementedException();
        }

        public Task<ApplicationRole> GetRoleByNormalizedName(string normalizedRoleName)
        {
            throw new NotImplementedException();
        }

        public Task<ApplicationUser> GetUserById(string userId)
        {
            throw new NotImplementedException();
        }

        public Task<ApplicationUser> GetUserByNormalizedUserName(string normalizedUserName)
        {
            throw new NotImplementedException();
        }

        public async Task<ApplicationUser> GetUserByNormalizedUserNameAndPassword(string normalizedUserName, string password)
        {
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            httpClient.DefaultRequestHeaders.Accept.Add(contentType);

            var credentialBytes = Encoding.UTF8.GetBytes($"{normalizedUserName}:{password}");
            var base64Credential = Convert.ToBase64String(credentialBytes);

            this.httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64Credential);

            var response = await httpClient.GetAsync(Infrastructure.API.User.GetUserByName(BaseUri, normalizedUserName));
            if (!response.IsSuccessStatusCode && response.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                logger.LogError("Could not retrieve {user}", normalizedUserName);
                return null;
            }
            var stringData = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<ApplicationUser>(stringData);
        }

        public Task<IList<ApplicationUser>> GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public Task UpdateRole(ApplicationRole role)
        {
            throw new NotImplementedException();
        }

        public Task UpdateUser(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            this.httpClient.Dispose();
        }
    }
}
