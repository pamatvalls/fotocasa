﻿using FotocasaTechnicalTest.IDP.API.Context;
using FotocasaTechnicalTest.IDP.API.Managers;
using FotocasaTechnicalTest.IDP.API.Stores;
using FotocasaTechnicalTest.IDP.Common.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("FotocasaTechnicalTest.IDP.API.Tests")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2, PublicKey=0024000004800000940000000602000000240000525341310004000001000100c547cac37abd99c8db225ef2f6c8a3602f3b3606cc9891605d02baa56104f4cfc0734aa39b93bf7852f7d9266654753cc297e7d2edfe0bac1cdcf9f717241550e0a7b191195b7667bb4f64bcb8e2121380fd1d9d46ad2d92d2d15605093924cceaf74c4861eff62abf69b9291ed0a340e113be11e6a7d3113e92484cf7045cc7")]
namespace FotocasaTechnicalTest.IDP.API
{
    public static class ServicesExtensions
    {
        public static IServiceCollection AddAPIIdentity(this IServiceCollection services)
        {
            services.AddDefaultIdentity<ApplicationUser>(config =>
            {
                config.Password.RequireLowercase = false;
                config.Password.RequiredLength = 5;
                config.Password.RequireNonAlphanumeric = false;
                config.Password.RequireDigit = false;
            })
            .AddDefaultUI(UIFramework.Bootstrap4)
                .AddSignInManager<ApiSignInManager>()
                .AddUserManager<ApiUserManager>()
                .AddRoles<ApplicationRole>()
                .AddUserStore<ApiUserStore>()
                .AddRoleStore<ApiRoleStore>();

            services.AddScoped(typeof(ICustomUserStore<>).MakeGenericType(typeof(ApplicationUser)), typeof(ApiUserStore));
            services.AddHttpContextAccessor();
            services.TryAddScoped<SignInManager<ApplicationUser>>();
            services.TryAddScoped<IContext, ApiContext>();
            services.AddHttpClient<IContext, ApiContext>();

            return services;
        }
    }
}
