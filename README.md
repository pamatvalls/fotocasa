# Introduction

Technical Test for .Net Backend position at Schibsted Media Group.
The solution includes an API, and an MVC frontal. The API serves as the single entry point to the authentication.

Custom SignInManager and Stores have been made for the web so Identity logs against the API.

The API also uses identity with modified stores to go against an InMemory non-persistent-through-executions context.

## Docker Container

You can run the Web sample by running these commands from the root folder (where the .sln file is located):

```sh
docker-compose build
docker-compose up
```

### *fotocasatechnicaltest.api*

The api, located at the port 44377, you can browse the main url for the swagger ui.

### *fotocasatechnicaltest.web*

The frontend, located at the port 44343

#### Pages
* home/index
	roles with Access:
	* ADMIN
	* PAGE_1
	* PAGE_2
	* PAGE_3
	
* home/page1
	roles with Access:
	* ADMIN
	* PAGE_1
	
* home/page2
	roles with Access:
	* ADMIN
	* PAGE_2
	
* home/page3
	roles with Access:
	* ADMIN
	* PAGE_3

### *SEQ*

The log aggregator, located at the port 15341

## Tests

You can run the tests executing with dotnet-cli executing the following commands:

```sh
dotnet test /p:CollectCoverage=true /p:Exclude=[xunit.*]* ./FotocasaTechnicalTest/FotocasaTechnicalTest.sln
```

## Users
These are the four users that are created everytime that you re-rerun the application

### ADMIN

	user: ADMIN
	password: ADMIN
	role: ADMIN
	
### PAGE_1

	user: PAGE_1
	password: PAGE_1
	role: PAGE_1
	
### PAGE_2

	user: PAGE_2
	password: PAGE_2
	role: PAGE_2
	
### PAGE_3

	user: PAGE_3
	password: PAGE_3
	role: PAGE_3